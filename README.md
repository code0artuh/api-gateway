## Api-Gateway

# express-http-proxy
  - Api gateway utilizando http proxy, responsavel por consumir diversas api's e distribuir em uma unica api baseando-se em rotas.
  
# api-gateway consumindo de apitest
As api's consumidas devem ser declaradas com a url e nome da rota no arquivo config.yml

## Dependencias.

  - express
  - @types/express
  - express-http-proxy
  - morgan
  - helmet

## Dependencias dev.

  - @types/express-http-proxy
  - @types/helmet
  - @types/morgan
  - ts-node-dev
  - typescript