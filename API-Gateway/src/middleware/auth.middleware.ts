import { Request, Response } from "express";
import endpoint from "../../config/ambiente";
class rotamiddleware{

    public rotaautorizar(req: Request, res: Response, next: Function){
        if(req.headers.url == endpoint){
        next()
        }else{
        res.status(401).json({ message: "Não autorizado" })
        }
    }
}

export default new rotamiddleware()