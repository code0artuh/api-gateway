import * as express from "express"
import * as helmet from "helmet"
import * as logger from "morgan"
import * as httpProxy from "express-http-proxy"
import rotamiddleware from "./middleware/auth.middleware"
import endpoint from "../config/ambiente"
const app = express()
app.use(helmet())
app.use(logger("dev"))
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.use("/users", rotamiddleware.rotaautorizar, httpProxy(endpoint, { timeout: 3000 }))

app.get("/", (req, res) => {
  return res.json({ message: "Bem-Vindo a GCB Investimetos" } )
})

app.listen(3000, () => {
  console.log("rodando")
})
