import * as path  from "path"
import * as dotenv from "dotenv";
dotenv.config({ path: path.resolve(__dirname, "../src/middleware/.env") });

export default process.env.USER_URL;