/* eslint-disable semi */
import * as express from 'express';
import * as helmet from 'helmet';
import * as logger from 'morgan';

const app = express();

app.use(helmet());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get('/', (req, res) => {
return res.json({message: 'Bem-vindo ao AcademyTech'});
});

app.listen(3001, () => {console.log("rodando")});